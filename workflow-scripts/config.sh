#!/bin/bash

export API_HOST=${API_HOST:-"https://processing.adgenomics.org"}
export API_TRAK=${API_TRAK:-"/v1/track/by-run"}
export URI="$API_HOST$API_TRAK"
