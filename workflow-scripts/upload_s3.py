#!/usr/bin/env python3

import boto3
from boto3.s3.transfer import TransferConfig
import argparse
import os

# setup
config = TransferConfig(use_threads=False)
s3 = boto3.client('s3')


# get options
parser = argparse.ArgumentParser(description='Upload to S3 location')
parser.add_argument('--file', type=str, required=True)
parser.add_argument('--upload_path', type=str, required=True)
args = parser.parse_args()
filename = args.file
s3path = args.upload_path

# parse s3path
bucket = s3path.split('/')[2]
obj_path = s3path.split('/')[3:]
obj_key = '/'.join(obj_path)


s3.upload_file(filename, bucket, obj_key,  Config=config)
print("Uploaded {} to s3://{}/{}".format(filename, bucket, obj_key))
