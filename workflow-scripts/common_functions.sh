#!/bin/bash

# @param $1 - urlencoded string
# @return - decoded string
#
function rawurldecode
{
    printf $(printf '%b' "${1//%/\\x}")
}

#
# twice url encoding for web urls
# @param $1 - string to be encoded
# @return - urlencoded *2 string
#
function rawurlencode(){
  echo -n "$1" | perl -pe 's/([^a-zA-Z0-9_.!~*()'\''-])/sprintf("%%%02X", ord($1))/ge' | perl -pe 's/(\W)/sprintf("%%%02X", ord($1))/ge'
}

function get_run(){
  if [ "$ENABLE_DB" != "1" ];then
    echo -n 0
    return 0
  fi

  if [ ! -z $RID ];then
    echo -n $RID
    return 0
  fi

  local PRJ_ID=$(get_project_id)
  local SM=$(get_sample_name)

  [[ -z "$PRJ_ID" ]] && echoerr "Missing \$PRJ_ID" && kill -INT $$
  [[ -z "$SM" ]] && echoerr "Missing \$SM" && kill -INT $$

  echo -n $(curl -sS "$API_HOST/v1/track/get-run/$PRJ_ID/$SM" | grep -Po 'id":"[^"]+"' | sed 's/id":"\|"//g')
}

function get_sample_name(){
  if [ "$ENABLE_DB" != "1" ];then
    echo -n 0
    return
  fi

  if [ ! -z $SM ];then
    echo -n $SM
    return 0
  fi

  local HOST=$(detect_hostname)

  if [ -z $HOST ];then
    echoerr "Error: Missing HOST! Can not continue"
    error-to-otto "Missing HOST! Can not continue"
    kill -INT $$
  else
    local H=$(echo $HOST|grep -Po "P\d+\S+"|sed 's/P[0-9]\+-//'|sed 's/-master//')
    # make exception for ADNI naming error
    if [[ "$H" == ADNI-* ]];then
      H=$(echo $H|sed 's/-/_/g')
    elif [[ "$H" == LP* ]];then
      H=$(echo $H|sed 's/DNA-/DNA_/g')
    fi
    echo -n $H
  fi
}

function get_project_id(){
  if [ "$ENABLE_DB" != "1" ];then
   echo -n 0
   return
  fi

  if [ ! -z $PRJ_ID ];then
   echo -n $PRJ_ID
   return 0
  fi

  local HOST=$(detect_hostname)

  if [ -z $HOST ];then
    echoerr "Error: Missing HOST! Can not continue"
    error-to-otto "Missing HOST! Can not continue"
    kill -INT $$
  else
    echo -n $(echo $HOST|grep -m 1 -Po  "\-P\d+" | head -1 | sed 's/-P//')
  fi
}

function get_s3_results_location(){
  if [ "$ENABLE_DB" != "1" ];then
   return
  fi

  local PRJ_ID=$1
  local SM=$2
  [[ -z "$PRJ_ID" ]] && echoerr "Missing \$PRJ_ID" && kill -INT $$
  [[ -z "$SM" ]] && echoerr "Missing \$SM" && kill -INT $$
  local RID=$(get_run)

  local S3PATH=$(curl -sS "$API_HOST/v1/sample/get-attr/results_s3_uri/$PRJ_ID/$SM"|grep -Po 'value":"[^"]+'|sed 's/value":"//'|sed 's#/$##')
  if [ ! -z "$S3PATH" ];then
    local URI=$(rawurldecode $S3PATH)
    URI=$(echo $URI|sed "s/_RID_/$RID/")
    echo -n $URI
  else
    echoerr "Empty \$S3PATH"
  fi
}

function track_item(){
 if [ "$ENABLE_DB" != "1" ];then
   return
 fi

 local TRACK_NAME="$1"
 local VAL="$2"
 local RID=$(get_run)

 [[ -z "$RID" ]] && echoerr "Missing \$RID" && kill -INT $$

 echo $(curl -sS "$URI/$RID/$TRACK_NAME/$VAL")

}
