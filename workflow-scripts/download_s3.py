#!/usr/bin/env python3

import boto3
from boto3.s3.transfer import TransferConfig
import argparse
import os

# setup
config = TransferConfig(use_threads=False)
s3 = boto3.client('s3')


# get options
parser = argparse.ArgumentParser(description='Download from S3 bucket')
parser.add_argument('--file', type=str, required=True)
parser.add_argument('--download_dir', type=str, required=False, default='/scratch')
args = parser.parse_args()
s3path = args.file
dpath = args.download_dir

# parse s3path
bucket = s3path.split('/')[2]
obj_path = s3path.split('/')[3:]
obj_key = '/'.join(obj_path)
filename = os.path.join(dpath,s3path.split('/')[-1])

if not os.path.exists(filename):
  os.makedirs(dpath, exist_ok=True)
  s3.download_file(bucket, obj_key, filename, Config=config)
else:
  print("File exists")
