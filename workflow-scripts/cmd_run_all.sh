#!/usr/bin/env bash

set -euxo pipefail

source /workflow-scripts/config.sh
source /workflow-scripts/common_functions.sh

echo $SM $PRJ_ID
export RID=$(get_run)

out=done.$SM.txt
cores=${MAX_THREADS:-$(nproc)}


# grab storage
if [ "$GETEBS" == "1" ];then
   if [ ! -z "$EBSSIZE" ];then
     echo $(($EBSSIZE * 1024 * 1024 * 1024)) > /TOTAL_SIZE
   else
     # calculate storage
     SZ=0
     for I in $INFILE;do
         SZ=$(( $($CLD_LS "$I" | awk '{print $3}') +  $SZ))
     done
     echo $(($SZ * 5 )) > /TOTAL_SIZE
   fi

   # wait for ecs-manager to mount EBS
   for i in {1..20};do
      if mountpoint -q -- "/scratch"; then
         break
      fi
      sleep 1m
   done

   if ! mountpoint -q -- "/scratch";then
       echo "Container ERROR"
       exit 1
   fi

   df -h
fi


snakemake --cores $cores \
            --snakefile /workflows/Snakefile-two-SV-callers \
            --configfile /workflow-scripts/config.yaml \
            --directory /scratch \
           --nocolor \
             $out

df -h
