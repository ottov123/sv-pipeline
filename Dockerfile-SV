FROM ubuntu:20.04 AS builder-tools
LABEL maintainer="Otto Valladares <ottov.upenn@gmail.com>"
LABEL version="1.0"
LABEL description="UPENN SV Pipeline."

ARG DEBIAN_FRONTEND="noninteractive"
RUN apt-get update && apt-get -y install wget g++ make autoconf libtool pkg-config \
       libgsl-dev zlib1g-dev libbz2-dev liblzma-dev \
       libcurl4 libcurl4-openssl-dev  libssl-dev libnlopt-cxx-dev

# Download tools
RUN wget --no-verbose --output-document=- "https://github.com/samtools/htslib/archive/1.10.2.tar.gz" | tar -xzf - && \
    wget --no-verbose --output-document=- "https://github.com/samtools/bcftools/releases/download/1.10.2/bcftools-1.10.2.tar.bz2" | tar -xjf - && \
    wget --no-verbose --output-document=- "https://github.com/samtools/samtools/releases/download/1.10/samtools-1.10.tar.bz2" | tar -xjf - && \
    wget --no-verbose --output-document=- "https://github.com/gymreklab/GangSTR/archive/v2.4.3.tar.gz" | tar -xzf -  && \
    wget --no-verbose --output-document=- "https://github.com/ottov/strelka-py3/releases/download/v2.9.10-py3/strelka-2.9.10-py3.centos6_x86_64.tgz" | tar -xzf - && \
    wget --no-verbose --output-document=- "https://github.com/Illumina/ExpansionHunter/releases/download/v3.2.2/ExpansionHunter-v3.2.2-linux_x86_64.tar.gz" | tar -xzf - && \
    wget --no-verbose --output-document=- "https://github.com/ottov/manta-py3/releases/download/1.6.0-py3/manta-1.6.0-py3.centos6_x86_64.tgz" | tar -xzf - && \
    wget --no-verbose "https://github.com/brentp/smoove/releases/download/v0.2.5/smoove" && \
    wget --no-verbose "https://github.com/brentp/mosdepth/releases/download/v0.2.9/mosdepth" && \
    wget --no-verbose --output-document=/gsort "https://github.com/brentp/gsort/releases/download/v0.1.4/gsort_linux_amd64" && \
    wget --no-verbose --output-document=- "https://github.com/arq5x/lumpy-sv/releases/download/0.3.0/lumpy-sv.tar.gz" | tar -xzf - && \
    wget --no-verbose --output-document=- "https://github.com/ottov/svtyper-py3/releases/download/v0.7.1-py3/svtyper-py3-v0.7.1.tar.gz" | tar -xzf - && \
    wget --no-verbose "https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh"

# compile tools
RUN cd /htslib-1.10.2/ && autoconf && autoheader && ./configure --enable-s3 && make install -j2 && \
    cd /bcftools-1.10.2/ && autoconf && autoheader && ./configure --enable-libgsl --with-htslib=system && make -j2 && \
    cd /samtools-1.10/ && autoheader && autoconf -Wno-syntax && ./configure --without-curses --with-htslib=system && make -j2 && \
    cd /GangSTR-2.4.3/ && sed -i 's/libtool/libtoolize/g' reconf && ./reconf && \
       ./configure && sed -i 's/#include <algorithm>/#include <algorithm>\n#include <unistd.h>/' src/bam_io.h && make -j2 && \
    cd /lumpy-sv && make -j2

##
# Next Stage
##
FROM ubuntu:20.04 AS build-conf

ARG DEBIAN_FRONTEND="noninteractive"
RUN apt-get update && apt-get -y install make gcc  \
       libgsl-dev zlib1g-dev libbz2-dev liblzma-dev \
       libcurl4 libcurl4-openssl-dev libssl-dev libnlopt-cxx-dev  \
       curl nano && \
     rm -fR /var/cache/apt /var/lib/apt/lists && \
     mkdir -p /tools

COPY --from=builder-tools  /usr/local/lib/libhts.so /lib/libhts.so.3
COPY --from=builder-tools  /htslib-1.10.2/bgzip /htslib-1.10.2/tabix /tools/
COPY --from=builder-tools  /bcftools-1.10.2/bcftools /samtools-1.10/samtools /tools/
COPY --from=builder-tools  /GangSTR-2.4.3/src/GangSTR /tools/
COPY --from=builder-tools  /strelka-2.9.10.centos6_x86_64 /tools/strelka-2.9.10.centos6_x86_64
COPY --from=builder-tools  /ExpansionHunter-v3.2.2-linux_x86_64 /tools/ExpansionHunter-v3.2.2-linux_x86_64
COPY --from=builder-tools  /manta-1.6.0.centos6_x86_64 /tools/manta-1.6.0.centos6_x86_64
COPY --from=builder-tools  /Miniconda3-latest-Linux-x86_64.sh /tools/
COPY --from=builder-tools  /lumpy-sv/bin/ /tools/
COPY --from=builder-tools  /smoove /mosdepth /gsort /tools/
COPY --from=builder-tools  /svtyper-py3 /tools/svtyper-py3

ENV PATH /opt/conda/bin:/tools:$PATH

RUN bash /tools/Miniconda3-latest-Linux-x86_64.sh -b -p /opt/conda && \
    rm -fv /tools/Miniconda3-latest-Linux-x86_64.sh && \
    /opt/conda/bin/conda install --yes -c bioconda -c conda-forge snakemake==5.20.1 && \
    chmod +x /tools/smoove /tools/mosdepth /tools/gsort && \
    cd /tools/svtyper-py3/ && make install && \
    apt-get remove --purge --autoremove -y make gcc

ADD workflows  /workflows
ADD workflow-scripts   /workflow-scripts
